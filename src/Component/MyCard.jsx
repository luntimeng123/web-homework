import React, { Component } from "react";
import { Card, Button } from "react-bootstrap";
import Counter from "./Counter";

export default class MyCard extends Component {
  constructor() {
    super();

    this.state = {
      product: [
        {
          image:
            "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRlgER76qF7DeCaZoruPdIyxEJIeSp0tja95g&usqp=CAU",
          title: "Black Panther",
          des:
            "Desktop and Mobile Phone Wallpaper 4K Black Panther Chadwick Boseman",
        },
        {
          image:
            "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTE1HqyikC6pVynmA3vC6s13Dqdp72Lr4vUYQ&usqp=CAU",
          title: "Dead Pool",
          des:
            "Deadpool is a fictional character appearing in American comic books published by Marvel Comics.",
        },
        {
          image:
            "https://c4.wallpaperflare.com/wallpaper/151/1000/60/spider-man-chibi-marvel-comics-hd-wallpaper-preview.jpg",
          title: "Spider man",
          des:
            "A collection of the top 43 Chibi Spiderman wallpapers and backgrounds available for download for free",
        },
      ],
    };
  }

  //Remove element from ArrayProdect
  onRemove = (index)=>{

    const newProduct = [...this.state.product];
    newProduct.splice(index,1);

    this.setState(state =>({

      product:newProduct

    }));
    
  }
  

  render() {
    //map loop is js loop and it need function as parameter ,we can use arraow function in it

    let loopImange = this.state.product.map((item,index) => {
      return (
        <div>
          {
            <Card style={{ width: "18rem", margin: "10px auto" }}>
              <Card.Img variant="top" src={item.image} />
              <Card.Body>
                {/* loop title from Image object */}
                <Card.Title>{item.title}</Card.Title>
                {/* loop des from Image object */}
                <Card.Text>{item.des}</Card.Text>
                <Button variant="danger" onClick={()=>this.onRemove(index)}>
                  Delete
                </Button>
              </Card.Body>
            </Card>
          }

          <Counter />
        </div>
      );
    });

    return (
      <div style={{ display: "flex", justifyContent: "space-around" }}>
        {loopImange}
      </div>
    );
  }
}
