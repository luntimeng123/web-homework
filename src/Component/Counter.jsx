

import React, { Component } from 'react'

class Counter extends Component {

    //create constructor
    constructor(){

        super();//if we do not super keyword it will cause error
        
        //create state ,it private in java
        this.state = {
            count: 0,
        }
    }


    //create function,we can the name of funtion
    //we use arrow function because we no need to bin object

   onIncrease = ()=> {
       //method setSate use for update state child,state chile are everthing state
       
       this.setState(curr =>({
           count: curr.count+1
       }))

       //curr =>({}) = this.state
       //{} ,everthing write in are code javascript
       //() everthing in it are object

   }

   //function
   onDecrease = ()=>{

    this.setState(curr =>({
        count: curr.count-1
    }))
   }

    render() {



        return (
            <div>
              
                <p style={{textAlign:"center"}}>
                <button onClick={this.onIncrease}>+</button>
                <span style={{fontSize:"25px"}}>{" "+this.state.count+" "}</span>
                <button onClick={this.onDecrease}>-</button>
                </p>

            </div>
        )
    }
}

export default Counter;
