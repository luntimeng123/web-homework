import React from 'react'
import ReactROM from 'react-dom'
import App from './App'

//jsx = js + html ,we usually call it xml


ReactROM.render(

    <App />,
    document.getElementById('root')

)